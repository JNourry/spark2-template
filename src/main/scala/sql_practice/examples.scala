package sql_practice

import org.apache.spark.sql.Row
import org.apache.spark.sql.functions._
import spark_helpers.SparkSessionHelper

object examples {

  def nextInt(i: Int) = ???

  def exec1(): Unit ={
    val spark = SparkSessionHelper.getSparkSession()
    import spark.implicits._
    val tourDF = spark.read
        .option("multiline", true)
        .option("mode", "PERMISSIVE")
        .json("data/input/tours.json")
    tourDF.show

    // 1 - unique level of difficulties
    tourDF.groupBy("tourDifficulty").count().show()

    // 2 - min / max / avg all tour package
    tourDF.agg(min("tourPrice"), max("tourPrice"), avg("tourPrice")).show

    // 3 - min / max / avg each difficulty level
    tourDF.groupBy("tourDifficulty").agg(min("tourPrice"), max("tourPrice"), avg("tourPrice")).show()

    // 4 - min / max / avg duration
    tourDF.groupBy("tourDifficulty").agg(min("tourPrice"), max("tourPrice"), avg("tourPrice"), min("tourLength"), max("tourLength"), avg("tourLength")).show()

    // 5 - top 10 top tags
    val tourT = tourDF.select(explode(tourDF("tourTags"))).groupBy("col").count()
    tourT.sort(desc("count")).show(10)

    // 6 - relationship top 10 and difficulties
    val tourR = tourDF.select(explode(tourDF("tourTags")), tourDF("tourDifficulty")).groupBy("col", "tourDifficulty").count()
    tourR.sort(desc("count")).show(10)

    // 7 - min / max / avg relationship 6 -
    tourDF
      .select(explode(tourDF("tourTags")), tourDF("tourDifficulty"), tourDF("tourPrice"))
      .groupBy("col", "tourDifficulty")
      .agg(min($"tourPrice"), max($"tourPrice"), avg($"tourPrice"))
      .sort(desc("avg(tourPrice)"))
      .show
  }
}
